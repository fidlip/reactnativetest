package com.react_native_with_redux;

import android.content.Intent;

import com.reactnativenavigation.controllers.SplashActivity;
import android.widget.ImageView;
import android.view.View;

public class MainActivity extends SplashActivity {

    @Override
    public View createSplashLayout() {
        ImageView view = new ImageView(this);
        int resID = getResources().getIdentifier("background" , "drawable", getPackageName());
        view.setScaleType(ImageView.ScaleType.FIT_XY);
//        overridePendingTransition(0, R.anim.fade_out);

        view.setImageResource(resID);
        return view;
    }
}
