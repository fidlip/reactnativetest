import {ActionTypes} from '../actions/application';

const INITIAL_APP_STATE = { appInitiated: null, tabBased: null };

/**
 * Reducer pro obsluhu přihlašování
 * @param state aktuální stav aplikace
 * @param action prováděná akce
 * @returns nový stav aplikace
 */
export default (state, action) => {
  let newState;

  switch (action.type) {
    case ActionTypes.APP_INITIATED:
      newState = {...state, appInitiated: true};
      break;

    case ActionTypes.TOGGLE_TAB_BASED:
      newState = { ...state, tabBased: action.tabBased};
      break;

    default:
      newState = state || INITIAL_APP_STATE;
      break;
  }

  return newState;

};
