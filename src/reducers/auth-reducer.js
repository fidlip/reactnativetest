import {ActionTypes} from '../actions/auth';

const INITIAL_AUTH_STATE = { accessToken: null, authState: ActionTypes.LOGGED_OUT };

/**
 * Reducer pro obsluhu přihlašování
 * @param state aktuální stav aplikace
 * @param action prováděná akce
 * @returns nový stav aplikace
 */
export default (state, action) => {
  let newState;

  switch (action.type) {
    case ActionTypes.LOGGED_IN:
      newState = { accessToken: action.payload, authState: ActionTypes.LOGGED_IN, user: action.user };
      break;

    case ActionTypes.LOGGED_OUT:
      newState = { accessToken: null, authState: ActionTypes.LOGGED_OUT };
      break;

    case ActionTypes.LOGIN:
    case ActionTypes.LOGOUT:
      newState = { ...state, authState: action.type };
      break;

    default:
      newState = state || INITIAL_AUTH_STATE;
      break;
  }

  return newState;

};
