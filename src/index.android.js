require('promise/lib/rejection-tracking').enable(
    {allRejections: true}
);

import {createStore, applyMiddleware} from "redux";
import {Provider} from "react-redux";
import thunk from "redux-thunk";
import {Navigation} from 'react-native-navigation';
import Icon from "react-native-vector-icons/FontAwesome";
import {registerScreens} from './screens/index';
import reducers from "./reducers";
import actions from './actions';
import {navBarBackgroundColor} from "./basic-styles";

import Promise from "promise/setimmediate";

import {BackHandler, Alert} from 'react-native';

const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);
const store = createStoreWithMiddleware(
    reducers
);

registerScreens(store, Provider);


let oldToken,
    oldTabBased;

let homeIcon, starIcon, downloadIcon;

// Nahraj ikony a pak teprve vykresli aplikaci
Promise.all([
  Icon.getImageSource('home', 30),
  Icon.getImageSource('star', 30),
  Icon.getImageSource('download', 30),
]).then(function(icons) {
  homeIcon = icons[0];
  starIcon = icons[1];
  downloadIcon = icons[2];

  onStoreUpdate();
  store.subscribe(onStoreUpdate);
});


function reinitializeApp(authState, tabBased, user) {
  if (authState !== actions.auth.ActionTypes.LOGGED_IN) {
    startWelcomeScreen();

  } else {

    if (!tabBased) {
      startSingleScreenApp(user);
    } else {
      startTabBasedApp(user);
    }
  }

  // odchytnutí HW tlačítka zpět
  BackHandler.addEventListener('hardwareBackPress', function() {
    Alert.alert(
        'Zpět',
        'Stisknuto tlačítko zpět',
        [{text: 'OK', onPress: () => console.log('OK Pressed')}]
    );
    return true;
  });


}

function startWelcomeScreen() {
  Navigation.startSingleScreenApp({
    screen: {
      screen: 'evidence.WelcomeScreen',
      title: 'DATASYS',
    },
    drawer: {
      left: {
        screen: "evidence.LoginMenu",
      },
    },
    animationType: "none",

  });
}

function startSingleScreenApp(user) {
  Navigation.startSingleScreenApp({
    screen: {
      screen: 'evidence.MainScreen',
      title: user.name,
    },
    drawer: {
      left: {
        screen: "evidence.MainMenu"
      },
    },
    animationType: "none",

  });
}

function startTabBasedApp() {
  Navigation.startTabBasedApp({
    tabs: [
      {
        label: 'Home',
        screen: 'evidence.HomeScreen',
        icon: homeIcon,
        title: 'Home'
      },
      {
        label: 'Starred',
        screen: 'evidence.StarredScreen',
        icon: starIcon,
        title: 'Starred'
      },
      {
        label: 'Downloads',
        screen: 'evidence.DownloadsScreen',
        icon: downloadIcon,
        title: 'Downloads'
      },
    ],
    appStyle: {
      tabBarBackgroundColor: navBarBackgroundColor,
      tabBarButtonColor: '#c6c1ff',
      tabBarSelectedButtonColor: '#eae7ff',
      initialTabIndex: 0,
    },
    drawer: {
      left: {
        screen: "evidence.MainMenu"
      },
    },
    animationType: "none",
  });
}



function onStoreUpdate() {
  let state = store.getState();

  let auth = state.auth;
  let token = auth.accessToken;
  let tabBased = state.application.tabBased;
  let user = auth.user;

  if (token !== oldToken || tabBased !== oldTabBased) {
    oldToken = token;
    oldTabBased = tabBased;
    reinitializeApp(auth.authState, tabBased, user);

  }

}

