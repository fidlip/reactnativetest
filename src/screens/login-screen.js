import * as React from 'react';
import {Button, StyleSheet, View, TextInput} from 'react-native';
import {PageStyles} from '../basic-styles';
import AppScreen from './app-screen';
import {connect} from 'react-redux';
import * as auth from '../actions/auth';
import actions from "../actions/index";
import Spinner from "react-native-loading-spinner-overlay";


class LoginScreen extends AppScreen {
  password = null;
  userName = null;

  constructor(props) {
    super(props);
    this.state = {};

  }

  render() {
    return (
        <View style={[PageStyles.basic, styles.container]}>
          <TextInput
              autoFocus={!this.state.focusPassword}
              style={styles.field}
              placeholder={"Uživatelské jméno"}
              onChangeText={this.onUserNameChange}
              onSubmitEditing={()=> this.refs["password"].focus()}
              returnKeyType="next"
          />
          <TextInput
              ref={"password"}
              autoFocus={this.state.focusPassword}
              secureTextEntry={true}
              style={styles.field}
              placeholder={"Heslo"}
              onChangeText={this.onPasswordChange}
              onSubmitEditing={this.onLogin}
          />
          <Button title="Login" onPress={this.onLogin} disabled={!this.state.valid}/>
          <Spinner visible={this.props.spinnerVisible} textContent={"Logging in ..."}/>
        </View>
    );
  }

  onLogin = () => {
    this.props.login(this.userName, this.password)
  };

  onUserNameChange = (userName) => {
    this.userName = userName;
    this.checkValidity();
  };

  onPasswordChange = (password) => {
    this.password = password;
    this.checkValidity();
  };

  checkValidity() {
    let valid = !!this.userName && !!this.password;
    if (!!this.state.valid !== valid)
      this.setState({valid});

  }

}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: 'center',
  },
  field: {
    color: '#000',
    fontSize: 25,
    width: 250,

  },
});


function mapStateToProps(state) {
  return { spinnerVisible: state.auth.authState === actions.auth.ActionTypes.LOGIN }
}

function mapDispatchToProps(dispatch) {
  return {
    login: (userName, password) => {
      dispatch(auth.login(userName, password));
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);