import * as React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {PageStyles} from '../basic-styles';
import {Screen} from 'react-native-navigation/src/Screen';
import Icon from "react-native-vector-icons/FontAwesome";


export default class LoginMenu extends Screen {

  render() {
    return (
        <View
            style={[PageStyles.sideMenu, styles.container]}
        >
          <Text style={PageStyles.menuItem} onPress={this.onLogin} >
            <Icon name={"hand-spock-o"} size={30}/>
            Přihlášení
          </Text>
          <Text style={PageStyles.menuItem} onPress={this.onAbout} >
            <Icon name={"glass"} size={30}/>
            O aplikaci
          </Text>
        </View>
    );
  }

  onLogin = () => {
    this.navigator.toggleDrawer({ side: "left" });

    this.navigator.push({
      screen: "evidence.LoginScreen",
      title: "Přihlašte se",
    });
  };

  onAbout = () => {
    this.navigator.toggleDrawer({side: "left"});

    this.navigator.showModal({
      screen: "evidence.AboutScreen",
    })
  }

}

const styles = StyleSheet.create({
  container: {
    padding: 40,
  },
});
