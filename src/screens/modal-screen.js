import {Screen} from 'react-native-navigation/src/Screen';
import {ModalNavBar} from "../basic-styles";

export default class ModalScreen extends Screen {
  static navigatorStyle = ModalNavBar;

}