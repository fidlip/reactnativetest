import {Screen} from 'react-native-navigation/src/Screen';
import {BasicNavBar} from '../basic-styles';

export default class AppScreen extends Screen {
  static navigatorStyle = BasicNavBar;

  // constructor(props) {
  //   super(props);
  //   this.navigator.setTitle({
  //     title: 'DATASYS',
  //   });
  //
  // }


  static navigatorButtons = {
    leftButtons: [
      {
        id: 'sideMenu' // id for this button, given in onNavigatorEvent(event) to help understand which button was clicked
      }
    ],
  };


}