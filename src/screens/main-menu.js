import * as React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {PageStyles} from '../basic-styles';
import {Screen} from 'react-native-navigation/src/Screen';
import {connect} from 'react-redux';
import Spinner from 'react-native-loading-spinner-overlay';
import actions from "../actions";
import Icon from "react-native-vector-icons/FontAwesome";


class MainMenu extends Screen {

  static defaultProps = {
    spinnerVisible: null,
    tabBased: false
  };

  render() {
    return (
        <View style={[PageStyles.sideMenu, styles.container]}>

          <Text style={PageStyles.menuItem} onPress={this.props.toggleTabBased}>
            <Icon name={"table"} size={30}>
              {this.props.tabBased ? "Single-Page" : "Tab-Based"}
            </Icon>
          </Text>
          <Text style={PageStyles.menuItem} onPress={this.props.onLogout}>
            <Icon name={"hand-lizard-o"} size={30}>
              Odhlášení
            </Icon>
          </Text>
          <Spinner visible={this.props.spinnerVisible} textContent={"Logging out ..."}/>
        </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    padding: 40,
  },
});

function mapStateToProps(state) {
  return {
    spinnerVisible: state.auth.authState === actions.auth.ActionTypes.LOGOUT,
    tabBased: state.application.tabBased
  }
}

function mapDispatchToProps(dispatch) {
  return {
    onLogout: () => {
      dispatch(actions.auth.logout());
    },
    toggleTabBased: () => {
      dispatch(actions.application.toggleTabBased())
    }

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainMenu);