import * as React from 'react';
import {Alert, Picker, StyleSheet, Text, TimePickerAndroid, Vibration, View} from 'react-native';
import {ModalStyles} from '../basic-styles';
import ModalScreen from "./modal-screen";
import Icon from "react-native-vector-icons/FontAwesome";

import * as Geolocation from "react-native/Libraries/Geolocation/Geolocation";


export default class AboutScreen extends ModalScreen {

  constructor(props) {
    super(props);
    this.navigator.setTitle({ title: "O aplikaci" });

  }

  render() {
    return (
      <View style={[ModalStyles.basic, styles.container]}>
          <Icon name={"info-circle"} size={60} />
          <Text style={styles.text}>Text o aplikaci</Text>

        <Text onPress={this.onInfo}>alert</Text>
        <Text onPress={this.onGeo}>geo</Text>
        <Text onPress={this.onTime}>time</Text>

        <Picker
            prompt={"Jazyk"}
            onValueChange={(itemValue, itemIndex) => this.setState({language: itemValue})}>
          <Picker.Item label="Java" value="java" />
          <Picker.Item label="JavaScript" value="js" />
          <Picker.Item label="C" value="js" />
          <Picker.Item label="Python" value="js" />
          <Picker.Item label="Ruby" value="js" />
          <Picker.Item label="PHP" value="js" />
        </Picker>

        <Text onPress={this.onVibrate}>vibrate</Text>
      </View>
    );
  }

  onInfo() {
    Alert.alert(
        'Informační alert',
        'Zpráva',
        [
          {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          {text: 'OK', onPress: () => console.log(chalk.yellow('OK Pressed'))},
        ],
        { cancelable: false }
    )
  }

  onGeo() {
    // Geolocation.requestAuthorization();
    Geolocation.getCurrentPosition((data) => {
      Alert.alert(
          'Geo',
          'Má pozice je' + JSON.stringify(data)
      )
    });
  }

  async onTime() {
    try {
      const {action, hour, minute} = await TimePickerAndroid.open({
        hour: 14,
        minute: 10,
        is24Hour: false,
      });
      if (action !== TimePickerAndroid.dismissedAction) {
        Alert.alert(
            'Time',
            'zvolili jsme' + hour + ":" + minute
        )
      }
    } catch ({code, message}) {
      console.warn('Cannot open time picker', message);
    }
  }

  onVibrate() {
    Vibration.vibrate(1000);
  }

}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: 'center',
  },
  text: {
    fontSize: 20,
  }
});
