import * as React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {PageStyles} from '../basic-styles';
import AppScreen from './app-screen';


export default class WelcomeScreen extends AppScreen {

  render() {
    return (
        <View
            style={[PageStyles.basic, styles.container]}
        >
          <Image source={{uri: "logo"}} style={PageStyles.logo} />
          <Text style={styles.textheader}>
            Testovací aplikace
          </Text>
        </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: 'center',
  },
  textheader: {
    marginTop: 50,
    color: '#000',
    fontSize: 30,
    textAlign: 'center',
  },
});
