import * as React from 'react';
import {Button, StyleSheet, Text, View} from 'react-native';
import {PageStyles} from '../basic-styles';
import AppScreen from './app-screen';


export default class MainScreen extends AppScreen {

  render() {
    return (
      <View style={[PageStyles.basic, styles.container]}>
          <Text style={styles.textheader}>
          Main single screen application!
        </Text>

        <Button title="Back" onPress={this.onBack}/>
      </View>
    );
  }

  onBack = () => {
      this.navigator.pop();
  }

}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: 'center',
  },
  textheader: {
    color: '#000',
    fontSize: 50,
    textAlign: 'center',
  },
});
