import { Navigation } from 'react-native-navigation';

import WelcomeScreen from './welcome-screen';
import LoginScreen from './login-screen';
import LoginMenu from './login-menu';
import MainScreen from './main-screen';
import MainMenu from './main-menu';
import HomeScreen from "./tab-screens/home-screen";
import StarredScreen from "./tab-screens/starred-screen";
import DownloadsScreen from "./tab-screens/downloads-screen";
import AboutScreen from "./about-screen";


export function registerScreens(store, provider) {
  Navigation.registerComponent('evidence.WelcomeScreen', () => WelcomeScreen, store, provider);
  Navigation.registerComponent('evidence.LoginScreen', () => LoginScreen, store, provider);
  Navigation.registerComponent('evidence.LoginMenu', () => LoginMenu, store, provider);
  Navigation.registerComponent('evidence.MainScreen', () => MainScreen, store, provider);
  Navigation.registerComponent('evidence.MainMenu', () => MainMenu, store, provider);
  Navigation.registerComponent('evidence.HomeScreen', () => HomeScreen, store, provider);
  Navigation.registerComponent('evidence.StarredScreen', () => StarredScreen, store, provider);
  Navigation.registerComponent('evidence.DownloadsScreen', () => DownloadsScreen, store, provider);
  Navigation.registerComponent('evidence.AboutScreen', () => AboutScreen, store, provider);
}