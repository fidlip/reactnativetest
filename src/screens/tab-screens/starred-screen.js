import * as React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {PageStyles} from '../../basic-styles';
import AppScreen from '../app-screen';


export default class StarredScreen extends AppScreen {

  render() {
    return (
      <View style={[PageStyles.basic, styles.container]}>
          <Text style={styles.textheader}>
          Druhá strana!
        </Text>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: 'center',
  },
  textheader: {
    color: '#000',
    fontSize: 50,
    textAlign: 'center',
  },
});
