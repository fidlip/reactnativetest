import * as React from 'react';
import {Button, StyleSheet, Text, View} from 'react-native';
import {PageStyles} from '../../basic-styles';
import AppScreen from '../app-screen';


export default class HomeScreen extends AppScreen {

  render() {
    return (
      <View style={[PageStyles.basic, styles.container]}>
          <Text style={styles.textheader}>
          Hlavní strana!
        </Text>

        <Button title="Tlač" onPress={this.onTlac}/>
      </View>
    );
  }

  onTlac = () => {
      this.navigator.pop();
  }

}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: 'center',
  },
  textheader: {
    color: '#000',
    fontSize: 50,
    textAlign: 'center',
  },
});
