export let ActionTypes = {
  LOGIN: "LOGIN",
  LOGGED_IN: "LOGGED_IN",
  LOGOUT: "LOGOUT",
  LOGGED_OUT: "LOGGED_OUT",
};

export function login(userName, password) {
    return async function(dispatch, getState) {
      dispatch({ type: ActionTypes.LOGIN});
      setTimeout(() => {
        dispatch({ type: ActionTypes.LOGGED_IN, payload: 'TESTOVACITOKEN123', user: {name: userName} });
      }, 500);
    }
}

export function logout() {
  return async function(dispatch, getState) {
    dispatch({ type: ActionTypes.LOGOUT});
    setTimeout(() => {
      dispatch({ type: ActionTypes.LOGGED_OUT});
    }, 500);
  }
}

export default {
  login: login,
  logout: logout,
  ActionTypes: ActionTypes,
}