import auth from "./auth";
import application from "./application";

export default {
  auth: auth,
  application: application,
}