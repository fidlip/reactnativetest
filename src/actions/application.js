export let ActionTypes = {
  APP_INITIATED: "@@redux/INIT",
  TOGGLE_TAB_BASED: "TOGGLE_TAB_BASED",
};


export function toggleTabBased(tabBased) {
    return async function(dispatch, getState) {
      dispatch({ type: ActionTypes.TOGGLE_TAB_BASED, tabBased: tabBased || !getState().application.tabBased });
    }
}

export default {
  toggleTabBased: toggleTabBased,
  ActionTypes: ActionTypes,
}