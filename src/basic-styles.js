import {StyleSheet} from "react-native";

export let pageBackgroundColor = "#5060A0";

export let menuBackgroundColor = "#405090";
export let menuItemColor = "#000";

export let navBarBackgroundColor = "#5566AA";

export let modalNavBarBgColor = "#9FAFEF";
export let modalBackgroundColor = "#8F9FDF";

export let BasicNavBar = {
    navBarBackgroundColor: navBarBackgroundColor,
};

export let ModalNavBar = {
    navBarBackgroundColor: modalNavBarBgColor,
};


export let PageStyles = StyleSheet.create({
  basic: {
    backgroundColor: pageBackgroundColor,
    flex: 1,
    flexDirection: "column",
  },
  sideMenu: {
    // padding: -50,
    width: 280,
    backgroundColor: menuBackgroundColor,
    flex: 1,
    flexDirection: "column",
  },
  menuItem: {
    color: menuItemColor,
    fontSize: 30,
    marginBottom: 20,
  },
  logo: {
    width: 192,
    height: 31,
  },
});

export let ModalStyles = StyleSheet.create({
  basic: {
    backgroundColor: modalBackgroundColor,
    flex: 1,
    flexDirection: "column",
  }
});