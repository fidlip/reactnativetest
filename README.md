# React Native boilerplate with Typescript and Redux
## (as well as react-navigation and native Facebook login button on both iOS and Android)

This project is aimed at people who want to quickly start developing their mobile Apps using React Native.

![Example gif](http://i.imgur.com/lmC4q4Y.gif)

## Installation
Before installing, make sure you are able to successfully run the base React Native project. See [React Native Getting Started Guide](http://facebook.github.io/react-native/docs/getting-started.html) for more information on how to do that.

If you are confident your environment is ready, you can proceed with cloning this project:

```sh
git clone https://github.com/fidlip/reactnativetest.git
```

Install dependencies:

```sh
npm i
```

Start React Native server:

```sh
npm start
```

Build the source-code with Typescript:

```sh
# Build once
npm build

# Build and watch for changes
npm build -- --watch
```


### Running
You may now want to try and run the App on either Android or iOS using one of the following commands:

#### Android

```sh
npm run android
```

#### iOS

```sh
npm run ios
```


### FAQ

1. [How can I rename the app?](https://stackoverflow.com/questions/32830046/renaming-a-react-native-project) 

